import React, {Component, useContext} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Index from './index.js';
import About from './about.js';
import Crut from './crud.js';
import './style.css';
import Logo from "./img/logo.png";
import {Context} from "./main.js"
import Login from "./login"

function Routers(props) {
    const login = true
    return (
        <Router>
        <div>
            {(login)?
                <nav className="header" style={{display:"flex",justifyContent:"space-between",flexDirection:"row"}}>
                    <img id="logo" src={Logo} width="200px" style={{marginRight:"15px"}} />                    
                <ul>
                    <li>
                        <Link to ="/index">Home </Link> 
                    </li>
                    <li>
                        <Link to ="/about">About </Link> 
                    </li>
                    <li>
                        <Link to ="/Crut">Movie List Editor </Link> 
                    </li>
                    <li>
                        <Link to="/login">Logout</Link>
                    </li>
                </ul>
            </nav>            
            :
            <nav className="header" style={{display:"flex",justifyContent:"space-between",flexDirection:"row"}}>
                <img id="logo" src={Logo} width="200px" style={{marginRight:"15px"}} />                    
                <ul>
                    <li>
                        <Link to ="/index">Home </Link> 
                    </li>
                    <li>
                        <Link to ="/about">About </Link> 
                    </li>
                    <li>
                        <Link to ="/login">Login </Link> 
                    </li>
                </ul>
            </nav>              
            }                       
            
            <Switch>
                <Route path="/index">
                    <div style={{marginTop:"1%"}}>
                        <Index />                      
                    </div>                
                </Route>
                <Route path="/about">
                    <div style={{marginTop:"1%"}}>
                        <About />                     
                    </div>                   
                </Route>
                <Route path="/Crut">
                    <div style={{marginTop:"1%"}}>
                        <Crut />                     
                    </div>                   
                </Route>
                <Route path="/login">
                    <div style={{marginTop:"1%"}}>
                        <Login />                     
                    </div>                   
                </Route>
            </Switch>          
        </div>
        </Router>
    );
}

export default Routers;