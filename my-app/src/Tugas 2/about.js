import React, {Component} from 'react';

const About = () => {
    return (
        <section>
            <div style={{padding: "10px", border: "1px solid #ccc"}}>
            <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <ol>
                <li><strong style={{width: "100px"}}>Nama:</strong> Gregorius Dimas Baskara</li> 
                <li><strong style={{width: "100px"}}>Email:</strong> gregdimasbaskara@gmail.com</li> 
                <li><strong style={{width: "100px"}}>Sistem Operasi yang digunakan:</strong> Windows 8</li>
                <li><strong style={{width: "100px"}}>Akun Gitlab:</strong> 16519111</li> 
                <li><strong style={{width: "100px"}}>Akun Telegram:</strong> @Gregorius Dimas</li> 
            </ol>
            </div>
            <br/>
            <br/>
        </section>                                
    );
}
export default About;