import React, {Component, useState, useEffect, createContext, useContext} from 'react';
import './style.css';
import {Context} from "./main.js"

function Tugas(props) {

    const setLogin = useContext(Context)
    console.log(setLogin)
    function logins(){
        setLogin(true)
    }
    
    return (
        <div className="App">
            <section>
                <div style={{display:"flex", justifyContent: "center", alignItems: "center"}}>
                    <h3>Username : </h3>
                    <input type="text" style={{marginLeft: "1%"}} />
                </div>
                <div style={{display:"flex", justifyContent: "center", alignItems: "center"}}>
                    <h3>Password : </h3>
                    <input type="text" style={{marginLeft: "1%"}}/>
                </div>
                <div style={{display:"flex", justifyContent: "center", alignItems: "center"}}>
                    <button onClick={logins}>Login</button>
                </div>
            </section>
        </div>
    );
  }
  
  Tugas.getInitialProps = async function() {
    const res = await fetch
    ("http://backendexample.sanbercloud.com/api/fruits");
    const data = await res.json();

    return{
        dataBuah: data
    };
}

  export default Tugas