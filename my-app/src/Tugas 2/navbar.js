import React from 'react';
import './style.css';
import Logo from './img/logo.png'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Home from './index'
import About from './about'

function Index() {
    return (
          <div>
              <header>
                <img id="logo" src={Logo} width="200px"/>
                <Router>
                    <nav style={{marginBottom: "200px"}}>
                        <ul>
                            <li>
                                <Link to="/Home">Home</Link>
                            </li>
                            <li>
                                <Link to="/About">About</Link>
                            </li>
                            <li>
                                <Link to="/Tugas11">Login</Link>
                            </li>
                        </ul>
                    </nav>
                    <Switch>
                        <Route path="/Home">
                            <Home/>
                        </Route>
                        <Route path="/About">
                            <About/>
                        </Route>
                        <Route path="/Tugas11">

                        </Route>
                    </Switch>
                </Router>
              </header>
          </div>
    );
  }
  
  export default Index;
            