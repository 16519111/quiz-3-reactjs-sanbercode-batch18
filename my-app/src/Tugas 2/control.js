import React from "react"
import {Provider} from "./main"
import Login from "./login"
import Routers from "./route"
const Movie = () =>{
  return(
    <Provider>
        <Routers/>
        <Login/>
    </Provider>
  )
}

export default Movie