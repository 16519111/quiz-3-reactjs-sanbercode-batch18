import React, {Component} from 'react';
import './style.css';
import Logo from './img/logo.png'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import About from './about'
import axios from 'axios'

export default class Index extends Component{
    constructor(props){
        super(props)
        this.state = {
            daftarFilm: [],
            cek: 2
        };
    }

    componentDidMount(){
        let temp = this.state.daftarFilm
        let tampung = []
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
            tampung = res.data
            console.log(tampung)
            this.setState({daftarFilm: tampung, cek:true})
        })
        tampung.forEach((el,index)=>{ temp.push({id: el.id, created_at: el.created_at, updated_at: el.updated_at, title: el.title, description: el.description,
            year: el.year, duration: el.duration, genre: el.genre, rating: el.rating, review: el.review, image_url: el.image_url })}); 
        console.log(temp)
        console.log(tampung[0])
    }

 
    
    
    render(){
        return (
            <div>
                <section>
                    <h1>Daftar Film Film Terbaik</h1>
                    {
                        this.state.daftarFilm.map((value,index) => {
                            return (
                                <div className="article">
                                    <h1 style={{textAlign: "left"}}>{value.title}</h1>
                                    <div style={{display: "flex", flexDirection: "row"}}>
                                        <img src={value.image_url} style={{maxWidth: "700px"}}/>
                                        <div style={{marginLeft: "1%"}}>
                                            <h2>Rating : {value.rating}</h2>
                                            <h2>Durasi : {value.duration}</h2>
                                            <h2>Genre : {value.genre}</h2>
                                        </div>
                                    </div>
                                    <p><strong>deskripsi : </strong>{value.description}</p>
                                </div>
                            )
                      })
                    }  
                </section>
                
                              
            </div>
        );
    }
}