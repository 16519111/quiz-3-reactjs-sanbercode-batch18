import React, {useState, useEffect} from "react"
import axios from "axios"
import "./style.css"
import "./style_crud.css"

const DaftarBuah = () => {  
  const [daftarBuah, setDaftarBuah] =  useState(null)
  const [input, setInput]  =  useState({year: 2020, duration: 120, rating: 0})

  useEffect( () => {
    if (daftarBuah === null){
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
      .then(res => {
        setDaftarBuah(res.data.map(el=>{ return {id: el.id, created_at: el.created_at, updated_at: el.updated_at, title: el.title, description: el.description,
            year: el.year, duration: el.duration, genre: el.genre, rating: el.rating, review: el.review, image_url: el.image_url}} ))
      })
    }
  }, [daftarBuah])
  
  const handleDelete = (event) => {
    let id_movies = parseInt(event.target.value)

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id_movies}`)
    .then(() => {
      setDaftarBuah(null)
    })
          
    
  }
  
  const handleEdit = (event) =>{
    let id_movies = parseInt(event.target.value)
        
    axios.get(`http://backendexample.sanbercloud.com/api/movies/${id_movies}`)
    .then(res => {
      let el = res.data
      setInput({id: el.id, created_at: el.created_at, updated_at: el.updated_at, title: el.title, description: el.description,
        year: el.year, duration: el.duration, genre: el.genre, rating: el.rating, review: el.review, image_url: el.image_url})
    })
  
  }

  const handleChange = (event) =>{
    let typeOfInput = event.target.id

    switch (typeOfInput){
      case "id":
      {
        setInput({...input, id: event.target.value});
        break
      }
      case "title":
      {
        setInput({...input, title: event.target.value});
          break
      }      
      case "description":
        {
          setInput({...input, description: event.target.value});
            break
        }      
      case "year":
      {
        setInput({...input, year: event.target.value});
        break
      }
      case "duration":
      {
        setInput({...input, duration: event.target.value});
          break
      }      
      case "genre":
      {
        setInput({...input, genre: event.target.value});
          break
      }      
      case "rating":
      {
        setInput({...input, rating: event.target.value});
          break
      }
    default:
      {break;}
    }
  }

  const handleSubmit = (event) =>{
    // menahan submit
    event.preventDefault()

    let id = input.id
    let title = input.title
    let description = input.description
    let year = input.year
    let duration = input.duration
    let genre = input.genre
    let rating = input.rating

    if (input.id === null){        
      axios.post(`http://backendexample.sanbercloud.com/api/movies/`, {id, title, description, year, duration, genre, rating})
      .then(res => {
          setDaftarBuah([
            ...daftarBuah, 
            { id: res.data.id, 
              id,
              title,
              description, 
              year,
              duration,
              genre,  
              rating
            }])
      })
    }else{
      axios.put(`http://backendexample.sanbercloud.com/api/movies/${input.id}`, {id, title, description, year, duration, genre, rating})
      .then(() => {
          let dataBuah = daftarBuah.find(el=> el.id === input.id)
          dataBuah.id = id
          dataBuah.title=title
          dataBuah.description=description
          dataBuah.year = year
          dataBuah.duration=duration
          dataBuah.genre=genre
          dataBuah.rating = rating
          setDaftarBuah([...daftarBuah])
      })
    }

    // reset input form to default
    setInput({year: 2020, duration: 120, rating: 0})

  }

  return(
    <>
    <section>
      <div style={{display: "flex", justifyContent: "center"}}>
        <input  type="number" required id="rating" value={input.rating} onChange={handleChange}/>
        <button style={{ float: "right"}}>search</button>
      </div>
      <h1>Daftar Film</h1>
      <div style={{display: "flex", justifyContent: "center"}}>
        <table>
            <thead>
            <tr>
                <th>No</th>
                <th>Title</th>
                <th>Description</th>
                <th>Year</th>
                <th>Duration</th>
                <th>Genre</th>
                <th>Rating</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

                {
                daftarBuah !== null && daftarBuah.map((item, index)=>{
                    return(                    
                    <tr key={index}>
                        <td>{index+1}</td>
                        <td>{item.title}</td>
                        <td>{item.description}</td>
                        <td>{item.year} </td>
                        <td>{item.duration}</td>
                        <td>{item.genre}</td>
                        <td>{item.rating}</td>
                        <td>
                        <button onClick={handleEdit} value={item.id}>Edit</button>
                        &nbsp;
                        <button onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                    </tr>
                    )
                })
                }
            </tbody>
        </table>
      </div>
      {/* Form */}
      <h1>Movies Form</h1>

      <div style={{width: "50%", margin: "0 auto", display: "block"}}>
        <div style={{border: "1px solid #aaa", padding: "20px"}}>
          <form onSubmit={handleSubmit}>
            <label style={{float: "left"}}>
              <strong>Title :</strong>
            </label>
            <input style={{float: "right"}} type="text" required id="id" value={input.title} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                <strong>Description :</strong>
            </label>
            <input style={{float: "right"}} type="text" required id="year" value={input.description} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                <strong>Year :</strong>
            </label>
            <input style={{float: "right"}} type="number" required id="rating" value={input.year} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                <strong>Duration :</strong>
            </label>
            <input style={{float: "right"}} type="number" required id="rating" value={input.duration} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                <strong>Genre :</strong>
            </label>
            <input style={{float: "right"}} type="number" required id="rating" value={input.genre} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                <strong>Rating :</strong>
            </label>
            <input style={{float: "right"}} type="number" required id="rating" value={input.rating} onChange={handleChange}/>
            <br/>
            <br/>
            <label style={{float: "left"}}>
                <strong>Image URL :</strong>
            </label>
            <input style={{float: "right"}} type="number" required id="rating" value={input.image_url} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button style={{ float: "right"}}>submit</button>
            </div>
          </form>
        </div>
      </div>
      </section>
    </>
    
  )
}

export default DaftarBuah