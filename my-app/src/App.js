import React from 'react';
import "./Tugas 2/style.css"
import Soal_2 from './Tugas 2/index'
import NavBar from './Tugas 2/navbar'
import Home from './Tugas 2/index'
import About from './Tugas 2/about'
import Logo from './Tugas 2/img/logo.png'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Crut from './Tugas 2/crud'
import Login from './Tugas 2/control'
import Routers from './Tugas 2/route'

function App() {
  return (
          <div>
              <Routers/>  
          </div>
  );
}

export default App;
